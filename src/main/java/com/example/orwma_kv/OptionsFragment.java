package com.example.orwma_kv;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class OptionsFragment extends Fragment {

    private String title = "Options";
    TextView mTitle;

    public static Fragment newInstance() {
        OptionsFragment optionsFragment = new OptionsFragment();
        return optionsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_options, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTitle = (TextView) view.findViewById(R.id.tvOptionsFragmentTitle);
        mTitle.setText(title);

    }

    public void setTitle(String title) {
        this.title = title;
    }
}


