package com.example.orwma_kv;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DeckItem implements Parcelable {

    private Integer index;
    private String name;
    private List<CardItem> cards;

    public DeckItem(Integer index, String name) {
        this.cards = new ArrayList<>();
        this.index=index;
        this.name=name;
    }

    public DeckItem(Integer index, String name, List<CardItem>cards) {
        this.cards = new ArrayList<>();
        this.cards.addAll(cards);
        this.index=index;
        this.name=name;
    }

    public DeckItem(DeckItem deckItem){
        this.index=deckItem.getIndex();
        this.name = deckItem.getName();
        this.cards = new ArrayList<>();
        this.cards.addAll(deckItem.getCards());
    }

    protected DeckItem(Parcel in) {
        this.cards = new ArrayList<>();
        if (in.readByte() == 0) {
            index = null;
        } else {
            index = in.readInt();
        }
        name = in.readString();
    }

    public void addCard(CardItem card){
        cards.add(card);
    }

    public void removeCard(CardItem card){
        cards.remove(card);
    }

    public List<CardItem> getCards(){
        return new ArrayList<CardItem>(cards);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getIndex() {
        return index;
    }

    //PARCEABLE
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (index == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(index);
        }
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DeckItem> CREATOR = new Creator<DeckItem>() {
        @Override
        public DeckItem createFromParcel(Parcel in) {
            return new DeckItem(in);
        }

        @Override
        public DeckItem[] newArray(int size) {
            return new DeckItem[size];
        }
    };

    // "Dissolving" cards into indexes
    public ArrayList<Integer> getCardIndexes() {
        List<Integer>indexes=new ArrayList<Integer>();
        for(int i=0; i<cards.size();i++) {
            indexes.add(cards.get(i).getCardIndex());
        }
        return new ArrayList<Integer>(indexes);
    }
    public ArrayList<Integer> getCardDrawables() {
        List<Integer>indexes=new ArrayList<Integer>();
        for(int i=0; i<cards.size();i++) {
            indexes.add(cards.get(i).getDrawableIndex());
        }
        return new ArrayList<Integer>(indexes);
    }
}
