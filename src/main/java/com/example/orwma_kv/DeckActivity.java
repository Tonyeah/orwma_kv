package com.example.orwma_kv;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class DeckActivity extends AppCompatActivity implements CardItemClickListener {

    private static final int CARDACTIVITY_REQUEST_CODE = 10;

    DeckItem deckItem;
    DeckItem uneditedDeckItem;

    EditText mETDeckName;
    Button mRenameButton;
    Button mBackButton;
    Button mAddButton;
    Button mSaveButton;

    RecyclerView rvCards;
    CardCollectionRVAdapter rvCardsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck);

        List<CardItem> cardItems = new ArrayList<CardItem>();
        for(int i=0; i<getIntent().getIntegerArrayListExtra("cardIndexes").size(); i++){
            cardItems.add(new CardItem(getIntent().getIntegerArrayListExtra("cardIndexes").get(i),
                    getIntent().getIntegerArrayListExtra("cardDrawables").get(i)));
        }
        deckItem = new DeckItem(
                ((DeckItem) getIntent().getParcelableExtra("deckItem")).getIndex(),
                ((DeckItem) getIntent().getParcelableExtra("deckItem")).getName(),
                cardItems
        );
        uneditedDeckItem= new DeckItem(deckItem);
        setUpViews();
        mETDeckName.setText(deckItem.getName());
        setUpListeners();
        setupRecyclerView();
        showCardsFromDeck();
    }

    private void setUpViews() {
        mETDeckName = findViewById(R.id.etDeckName);
        mRenameButton = findViewById(R.id.btnRename);
        mBackButton = findViewById(R.id.btnBack);
        mAddButton = findViewById(R.id.btnAdd);
        mSaveButton = findViewById(R.id.btnSave);
        rvCards = findViewById(R.id.rvCards);
    }

    private void setupRecyclerView() {
        rvCards.setLayoutManager(new GridLayoutManager(this, 3));
        rvCardsAdapter = new CardCollectionRVAdapter(this);
        rvCards.setAdapter(rvCardsAdapter);
    }

    private void showCardsFromDeck(){
        rvCardsAdapter.addAll(deckItem.getCards());
    }

    private void startCardChooserActivity(){
        Intent intent = new Intent(this, CardChooserActivity.class);
        startActivityForResult(intent, 2);
    }

    private void setUpListeners(){
        mRenameButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

                deckItem.setName(mETDeckName.getText().toString());
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startCardChooserActivity();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                List<DeckItem> dl = JsonControllerSingleton.getInstance().loadDeckList(getBaseContext());
                List<DeckItem> dl_new = new ArrayList<DeckItem>();
                for(int i=0; i < dl.size();i++){
                    if(i == deckItem.getIndex()){
                        dl_new.add(new DeckItem(i,deckItem.getName(), new ArrayList<CardItem>(deckItem.getCards())));
                    }else{
                        dl_new.add(new DeckItem(dl.get(i).getIndex(), dl.get(i).getName(), new ArrayList<CardItem>(dl.get(i).getCards())));
                    }
                }
                JsonControllerSingleton.getInstance().saveDeckList(getBaseContext(), dl_new);
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2){
            List<Integer>indexes= new ArrayList<Integer>(data.getIntegerArrayListExtra("chosen_cards_indexes"));
            for(int i=0; i< indexes.size(); i++){
                deckItem.addCard(new CardItem(indexes.get(i), CardMasterSingleton.getInstance().getDrawableIndex(this, indexes.get(i))));
            }
            rvCardsAdapter.addAll(deckItem.getCards());
        }
    }

    @Override
    public void onCardItemClicked(CardItem cardItem) {
        Intent intent = new Intent(this, CardActivity.class);
        intent.putExtra("drawableIndex", cardItem.getDrawableIndex());
        startActivityForResult(intent, CARDACTIVITY_REQUEST_CODE);
    }
}