package com.example.orwma_kv;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CardChooserActivity extends AppCompatActivity implements CardItemClickListener, CardItemLongClickListener {

    private static final int CARDACTIVITY_REQUEST_CODE = 10;

    private List<CardItem> chosenCards;
    RecyclerView rvAllCards;
    TextView mTitle;
    Button mCancelButton;
    Button mConfirmButton;
    ChoosableCardsRVAdapter rvAllCardsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_cards);
        chosenCards = new ArrayList<CardItem>();
        setUpViews();
        setUpRecyclerView();
        setUpListeners();
        showAllCards();
    }

    private void setUpListeners() {
        mCancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                chosenCards.clear();
                endActivity();
            }
        });

        mConfirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                endActivity();
            }
        });
    }

    private void endActivity(){
        Intent intent=new Intent();
        intent.putExtra("chosen_cards_indexes", new ArrayList<Integer>(getAllChosenCardsIndexes()));
        setResult(2,intent);
        finish();
    }

    public ArrayList<Integer> getAllChosenCardsIndexes(){
        List<Integer>cardIndexes = new ArrayList<Integer>();
        for(int i=0; i< chosenCards.size(); i++){
            cardIndexes.add(chosenCards.get(i).getCardIndex());
        }
        Log.e("getAllChosenCardsIndexes", cardIndexes.toString());
        return new ArrayList<Integer>(cardIndexes);
    }

    private void setUpViews() {
        mTitle = findViewById(R.id.tvChooseCardsTitle);
        mCancelButton = findViewById(R.id.btnCancel);
        mConfirmButton = findViewById(R.id.btnConfirm);
        rvAllCards = findViewById(R.id.rvCardGrid);
    }

    private void setUpRecyclerView() {
        rvAllCards.setLayoutManager(new GridLayoutManager(this, 3));
        rvAllCardsAdapter = new ChoosableCardsRVAdapter(this, this);
        rvAllCards.setAdapter(rvAllCardsAdapter);
    }

    private void showAllCards(){
        ArrayList<CardItem> cardItems = new ArrayList<CardItem>();
        for(int i = 1; i <= CardMasterSingleton.getInstance().getTotalCardCount(this); i++){
            cardItems.add(new CardItem(i, CardMasterSingleton.getInstance().getDrawableIndex(this,i)));
        }
        rvAllCardsAdapter.addAll(cardItems);
    }

    @Override
    public void onCardItemClicked(CardItem cardItem) {
        Intent intent = new Intent(this, CardActivity.class);
        intent.putExtra("drawableIndex", cardItem.getDrawableIndex());
        startActivityForResult(intent, CARDACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onCardItemLongClicked(CardItem cardItem, boolean bIsChosen) {
        if(bIsChosen){
            chosenCards.add(cardItem);
        }else {
            if(chosenCards.contains(cardItem)){
                chosenCards.remove(cardItem);
            }
        }
    }
}
