package com.example.orwma_kv;

public interface DeckItemClickListener {
    void onDeckItemClicked(DeckItem deckItem);
}
