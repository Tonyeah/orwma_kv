package com.example.orwma_kv;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CardCollectionRVAdapter extends RecyclerView.Adapter<CardItemViewHolder> {

    List<CardItem> cardItems = new ArrayList<CardItem>();
    private CardItemClickListener listener;

    public CardCollectionRVAdapter(CardItemClickListener listener) {this.listener=listener;}

    @NonNull
    @Override
    public CardItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View imageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new CardItemViewHolder(imageView, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardItemViewHolder holder, int position) {
        holder.initialize(cardItems.get(position));
    }

    @Override
    public int getItemCount() {
        return cardItems.size();
    }

    public void addAll(List<CardItem>data){
        cardItems.clear();
        cardItems.addAll(data);
        notifyDataSetChanged();
    }

    public void clear(){
        int size = cardItems.size();
        cardItems.clear();
        notifyItemRangeRemoved(0, size);
    }
}
