package com.example.orwma_kv;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

//This class' purpose is similar to so-called "CustomViewHolder"
public class DeckItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private DeckItem deckItem;
    private ImageClickListener listener;
    private DeckItemClickListener deckItemClickListener;

    private TextView tvDeckName;
    private ImageButton ibtnDeleteButton;

    public DeckItemViewHolder(@NonNull View itemView, ImageClickListener listener, DeckItemClickListener deckItemClickListener) {
        super(itemView);
        setUpViews();

        this.listener = listener;
        this.deckItemClickListener = deckItemClickListener;
        tvDeckName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deckItemClickListener.onDeckItemClicked(deckItem);
            }
        });
    }

    private void setUpViews() {
        tvDeckName = itemView.findViewById(R.id.tvDeckName);
        ibtnDeleteButton = itemView.findViewById(R.id.ibtnDeleteButton);
        ibtnDeleteButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        listener.OnImageClick(getAdapterPosition());
    }

    public void initialize(DeckItem deckItem){
        this.deckItem = deckItem;
        setTextViewName();
    }

    public void setTextViewName(){
        tvDeckName.setText(deckItem.getName());
    }
}

