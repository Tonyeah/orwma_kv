package com.example.orwma_kv;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ChoosableCardViewHolder extends RecyclerView.ViewHolder {

    private CardItem card;
    private ImageView mCardImage;
    private boolean bIsChosen;

    private CardItemClickListener cardItemClickListener;
    private CardItemLongClickListener cardItemLongClickListener;

    public ChoosableCardViewHolder(@NonNull View itemView, CardItemClickListener cardItemClickListener, CardItemLongClickListener cardItemLongClickListener) {
        super(itemView);
        this.cardItemClickListener=cardItemClickListener;
        this.cardItemLongClickListener=cardItemLongClickListener;
        bIsChosen=false;
        mCardImage=itemView.findViewById(R.id.ivCardImage);
        mCardImage.setClickable(true);
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        mCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemClickListener.onCardItemClicked(card);
            }
        });

        mCardImage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(bIsChosen){
                    mCardImage.clearColorFilter();
                    bIsChosen=false;
                }else{
                    mCardImage.setColorFilter(Color.GREEN);
                    bIsChosen=true;
                }
                cardItemLongClickListener.onCardItemLongClicked(card, bIsChosen);
                return bIsChosen;
            }
        });
    }

    public void initialize(CardItem card){
        setCardItem(card);
        setImage();
    }

    private void setCardItem(CardItem card) {
        this.card = card;
    }

    private void setImage(){
        mCardImage.setImageResource(card.getDrawableIndex());
    }
}