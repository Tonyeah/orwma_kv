package com.example.orwma_kv;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class DeckListRVAdapter extends RecyclerView.Adapter<DeckItemViewHolder> {

    List<DeckItem> deckList = new ArrayList<DeckItem>();
    private ImageClickListener listener;
    private DeckItemClickListener deckItemClickListener;

    public DeckListRVAdapter(ImageClickListener listener, DeckItemClickListener deckItemClickListener) {
        this.listener=listener;
        this.deckItemClickListener=deckItemClickListener;
    }

    @NonNull
    @Override
    public DeckItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View deckItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.deck_item, parent, false);
        return new DeckItemViewHolder(deckItemView, listener, deckItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DeckItemViewHolder holder, int position) {
        holder.initialize(deckList.get(position));
    }

    @Override
    public int getItemCount() {
        return deckList.size();
    }

    public void addAll(Context context){
        deckList.clear();
        deckList.addAll(JsonControllerSingleton.getInstance().loadDeckList(context));
        notifyDataSetChanged();
    }

    public void add(Integer index, String name){
        deckList.add(new DeckItem(index, name));
        notifyItemInserted(deckList.size());
    }

    public void remove(int position){
        if(position>=0 && position<deckList.size()) {
            deckList.remove(position);
            notifyItemRemoved(position);
        }
    }
}
