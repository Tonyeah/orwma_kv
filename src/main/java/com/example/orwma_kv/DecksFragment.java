package com.example.orwma_kv;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class DecksFragment extends Fragment implements ImageClickListener, DeckItemClickListener{

    private static final int DECKACTIVITY_REQUEST_CODE = 55;
    private List<DeckItem>decks;
    private String title = "Decks";
    TextView mTitle;
    RecyclerView rvDeckList;
    DeckListRVAdapter rvDeckListAdapter;
    FloatingActionButton fabAddButton;

    public static Fragment newInstance() {
        DecksFragment decksFragment = new DecksFragment();
        return decksFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_decks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        decks = new ArrayList<>();
        if(!JsonControllerSingleton.getInstance().fileExists(getContext())){
            JsonControllerSingleton.getInstance().saveDeckList(getContext(),decks);
        }
        decks= JsonControllerSingleton.getInstance().loadDeckList(getContext());
        mTitle = (TextView) view.findViewById(R.id.tvDecksFragmentTitle);
        mTitle.setText(title);
        rvDeckList=view.findViewById(R.id.rvDeckList);
        fabAddButton=view.findViewById(R.id.fabAddDeck);
        setUpListeners();

        setupRecyclerView();
        //loadDeckList();
    }

    private void setUpListeners() {
        fabAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decks.add(new DeckItem(decks.size(), "new deck"));
                JsonControllerSingleton.getInstance().saveDeckList(getContext(), decks);
                rvDeckListAdapter.addAll(getContext());
            }
        });
    }

    private void setupRecyclerView() {
        rvDeckList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvDeckListAdapter = new DeckListRVAdapter(this, this);
        rvDeckList.setAdapter(rvDeckListAdapter);
        rvDeckListAdapter.addAll(getContext());
    }

    private void loadDeckList(){
        List<DeckItem>deckItems = new ArrayList<DeckItem>();
        deckItems = JsonControllerSingleton.getInstance().loadDeckList(getContext());
        decks.clear();
        decks.addAll(deckItems);
        rvDeckListAdapter.addAll(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDeckList();
    }

    @Override
    public void OnImageClick(int position) {
        rvDeckListAdapter.remove(position);
        decks.remove(position);
        JsonControllerSingleton.getInstance().saveDeckList(getContext(),decks);
    }

    @Override
    public void onDeckItemClicked(DeckItem deckItem){
        Intent intent = new Intent(getActivity(), DeckActivity.class);
        intent.putExtra("deckItem", deckItem);
        intent.putIntegerArrayListExtra("cardIndexes", new ArrayList<Integer>(deckItem.getCardIndexes()));
        intent.putIntegerArrayListExtra("cardDrawables", new ArrayList<Integer>(deckItem.getCardDrawables()));
        startActivityForResult(intent, DECKACTIVITY_REQUEST_CODE);
    }
}
