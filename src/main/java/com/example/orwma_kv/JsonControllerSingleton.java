package com.example.orwma_kv;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class JsonControllerSingleton {

    private static JsonControllerSingleton INSTANCE = null;

    private static final String filename= "deck_list.json";

    private JsonControllerSingleton(){}

    public static JsonControllerSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new JsonControllerSingleton();
        }
        return (INSTANCE);
    }

    public boolean fileExists(Context context) {
        File file = context.getFileStreamPath(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }

    public List<DeckItem> loadDeckList(Context context){
        String json = null;
        try {
            FileInputStream fileIn=context.openFileInput(filename);
            int size = fileIn.available();
            byte[] buffer = new byte[size];
            fileIn.read(buffer);
            fileIn.close();
            json = new String(buffer, "UTF-8");
        } catch (FileNotFoundException ex) {
            System.err.println("Doesn't exist");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson gson = new Gson();

        Type deckItemPojoType = new TypeToken<ArrayList<DeckItemPOJO>>(){}.getType();
        List<DeckItemPOJO> deckItemPOJOList = gson.fromJson(json, deckItemPojoType);
        List<DeckItem>deckItems = new ArrayList<DeckItem>();
        for(int i=0; i<deckItemPOJOList.size(); i++){
            deckItems.add(deckItemPOJOList.get(i).convertToDeckItem(context));
        }
        return deckItems;
    }

    public void saveDeckList(Context context, List<DeckItem>decks){
        List<DeckItemPOJO> items = new ArrayList<>();
        for(int i=0; i<decks.size(); i++){
            List<Integer>cardIndexes=new ArrayList<Integer>();
            for(int j=0; j<decks.get(i).getCards().size();j++){
                cardIndexes.add(decks.get(i).getCards().get(j).getCardIndex());
            }
            items.add(new DeckItemPOJO(decks.get(i).getIndex(),decks.get(i).getName(), new ArrayList<Integer>(cardIndexes)));
        }

        Gson gson = new Gson();
        try {
            FileOutputStream fileout = context.openFileOutput(filename, MODE_PRIVATE);
            OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
            outputWriter.write(gson.toJson(items));
            outputWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
