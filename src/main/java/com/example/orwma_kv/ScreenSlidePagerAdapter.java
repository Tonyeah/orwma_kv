package com.example.orwma_kv;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private static  final int NUM_PAGES = 3;

    public ScreenSlidePagerAdapter(@NonNull FragmentManager fm){
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return DecksFragment.newInstance();
            case 1:
                return CardsFragment.newInstance();
            case 2:
                return OptionsFragment.newInstance();
        }
        Log.e("ScreenSlidePagerAdapter: getItem()", "position out of bounds (position<0 || position>2)");
        return new Fragment();
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "Decks";
            case 1: return "Cards";
            case 2: return "Options";
        }
        return "Error:getPageTitle";
    }
}
