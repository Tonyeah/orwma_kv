package com.example.orwma_kv;

public interface CardItemClickListener {
    void onCardItemClicked(CardItem cardItem);
}
