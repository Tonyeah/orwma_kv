package com.example.orwma_kv;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ChoosableCardsRVAdapter extends RecyclerView.Adapter<ChoosableCardViewHolder> {

    List<CardItem> cardItems = new ArrayList<CardItem>();
    private CardItemClickListener cardItemClickListener;
    private CardItemLongClickListener cardItemLongClickListener;


    public ChoosableCardsRVAdapter(CardItemClickListener cardItemClickListener, CardItemLongClickListener cardItemLongClickListener) {
        this.cardItemClickListener=cardItemClickListener;
        this.cardItemLongClickListener=cardItemLongClickListener;
    }

    @NonNull
    @Override
    public ChoosableCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View imageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        return new ChoosableCardViewHolder(imageView,cardItemClickListener,cardItemLongClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ChoosableCardViewHolder holder, int position) {
        holder.initialize(cardItems.get(position));
    }

    @Override
    public int getItemCount() {
        return cardItems.size();
    }

    public void addAll(List<CardItem>data){
        cardItems.clear();
        cardItems.addAll(data);
        notifyDataSetChanged();
    }

    public void clear(){
        int size = cardItems.size();
        cardItems.clear();
        notifyItemRangeRemoved(0, size);
    }
}