package com.example.orwma_kv;

import android.content.Context;

public class CardMasterSingleton {

    private static CardMasterSingleton INSTANCE = null;

    private CardMasterSingleton(){

    }
    public static CardMasterSingleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CardMasterSingleton();
        }
        return(INSTANCE);
    }

    public Integer getDrawableIndex(Context context, int index){
        String name = "c" + Integer.toString(index);
        Integer drawableIndex = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return drawableIndex;
    }

    public Integer getTotalCardCount(Context context){
        Integer count=0;
        while(context.getResources().getIdentifier("c" + Integer.toString(count+1), "drawable", context.getPackageName())!=0) {
            count++;
        }
        return count;
    }
}
