package com.example.orwma_kv;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class CardActivity  extends AppCompatActivity {

    private ImageView mCardImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_item_popup);
        mCardImage=findViewById(R.id.fullScreenImage);
        mCardImage.setImageResource(getIntent().getIntExtra("drawableIndex" , -1));
        setUpListeners();
    }

    private void setUpListeners() {
        mCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
