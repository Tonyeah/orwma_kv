package com.example.orwma_kv;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CardItemViewHolder extends RecyclerView.ViewHolder {

    private CardItem card;
    private ImageView mCardImage;
    private CardItemClickListener cardItemClickListener;

    public CardItemViewHolder(@NonNull View itemView, CardItemClickListener listener) {
        super(itemView);
        this.cardItemClickListener=listener;
        mCardImage=itemView.findViewById(R.id.ivCardImage);
        mCardImage.setClickable(true);
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        mCardImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemClickListener.onCardItemClicked(card);
            }
        });
    }

    public void initialize(CardItem card){
        setCardItem(card);
        setImage();
    }

    private void setCardItem(CardItem card) {
        this.card = card;
    }

    private void setImage(){
        mCardImage.setImageResource(card.getDrawableIndex());
    }
}
