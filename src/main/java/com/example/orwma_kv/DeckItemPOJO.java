package com.example.orwma_kv;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class DeckItemPOJO {
    private Integer index;
    private String name;
    private List<Integer> cards;

    public DeckItemPOJO(Integer index, String name, ArrayList<Integer> cardIndexes){
        this.index=index;
        this.name  =name;
        this.cards = new ArrayList<Integer>(cardIndexes);
    }

    @NonNull
    @Override
    public String toString() {
        return Integer.toString(index) + ", "+ name + ", "+ cards.toString();
    }

    public DeckItem convertToDeckItem(Context context){
        DeckItem deckItem = new DeckItem(index, name);
        for(int i=0; i<cards.size(); i++){
            deckItem.addCard(new CardItem(cards.get(i), CardMasterSingleton.getInstance().getDrawableIndex(context, cards.get(i))));
        }
        return deckItem;
    }
}
