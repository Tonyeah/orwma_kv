package com.example.orwma_kv;

public class CardItem {

    private Integer cardIndex;
    private Integer drawableIndex;

    public CardItem(Integer cardIndex, Integer drawableIndex) {
        this.cardIndex=cardIndex;
        this.drawableIndex=drawableIndex;
    }

    public Integer getCardIndex() {
        return cardIndex;
    }

    public Integer getDrawableIndex() {
        return drawableIndex;
    }
}
