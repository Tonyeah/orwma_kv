package com.example.orwma_kv;

public interface CardItemLongClickListener {
    void onCardItemLongClicked(CardItem cardItem, boolean bState);
}
