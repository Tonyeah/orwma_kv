package com.example.orwma_kv;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CardsFragment extends Fragment implements CardItemClickListener {

    private static final int CARDACTIVITY_REQUEST_CODE = 10;

    private String title = "Cards";
    TextView mTitle;
    RecyclerView rvCardCollection;
    CardCollectionRVAdapter rvCardCollectionAdapter;
    private List<CardItem>cardItems;

    private Integer totalCardCount = 0;

    public static Fragment newInstance() {
        CardsFragment cardsFragment = new CardsFragment();
        return cardsFragment;
    }

    private void refreshTotalCardCount(){totalCardCount = getTotalCardCount();}

    private Integer getTotalCardCount(){
        Integer count=0;
        while(getResources().getIdentifier("c" + Integer.toString(count+1), "drawable", getActivity().getPackageName())!=0) {
            count++;
        }
        return count;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cards, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTitle = (TextView) view.findViewById(R.id.tvCardsFragmentTitle);
        mTitle.setText(title);

        totalCardCount = getTotalCardCount();
        cardItems = new ArrayList<CardItem>();
        cardItems = createCardItems();
        rvCardCollection=(RecyclerView)view.findViewById(R.id.rvCardGrid);
        setupRecyclerView();
        loadCardCollection();
    }

    private void setupRecyclerView() {
        rvCardCollection.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvCardCollectionAdapter = new CardCollectionRVAdapter(this);
        rvCardCollection.setAdapter(rvCardCollectionAdapter);
        rvCardCollectionAdapter.addAll(cardItems);
    }

    private void loadCardCollection() {
        rvCardCollectionAdapter.clear();
        rvCardCollectionAdapter.addAll(cardItems);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadCardCollection();
    }


    private ArrayList<CardItem> createCardItems(){
        ArrayList<CardItem> cardItems = new ArrayList<CardItem>();
        for(int i=1; i<=totalCardCount; i++){
            cardItems.add(new CardItem(i, CardMasterSingleton.getInstance().getDrawableIndex(getContext(), i)));
        }
        return cardItems;
    }

    @Override
    public void onCardItemClicked(CardItem cardItem) {
        Intent intent = new Intent(getActivity(), CardActivity.class);
        intent.putExtra("drawableIndex", cardItem.getDrawableIndex());
        startActivity(intent);
    }
}
